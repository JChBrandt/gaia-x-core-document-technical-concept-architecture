## Interconnection

Interconnection, rephrasing [EU 2002/19/EC directive](https://eur-lex.europa.eu/legal-content/EN/TXT/HTML/?uri=CELEX:32002L0019&from=EN), refers to the physical or logical connection between two or multiple [Nodes](#resource) that enables the traffic exchange among them.  In the context of telecommunications, interconnection can be implemented directly between different stakeholders or through dedicated interconnection points (e.g. IXPs).

The difference to the simple connection is that in case of interconnection we are speaking of a connectivity that involves several parties at once. It allows unification of interconnected parties into digital ecosystems. Moreover, it can exhibit special characteristics, such as latency and bandwidth guarantees, that go beyond the characteristics of a path over the public Internet.
