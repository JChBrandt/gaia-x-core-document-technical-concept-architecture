## Digital Sovereignty

Digital Sovereignty is the power to make decisions about how digital processes, infrastructures and the movement of data are structured, built and managed.

### references
- Gaia-X, TAD 2020 p.3
