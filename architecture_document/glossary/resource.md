## Resource

A Resource is an internal building block, not available for order, used to compose [Service Offerings](#service-offering).   

Resource Categories include:

   - Data Resource, which consists of data (which may include derived data) in any form and includes the necessary information for data sharing.
   
   - Software Resource, consisting of non-physical functions.
   
   - Node, representing a computational or physical entity that hosts, manipulates, or interacts with other computational or physical entities.
   
   - Interconnection, which includes details of the connection between two or more Nodes.

Prominent attributes of a Resource are the location - physical address, Autonomous System Number, network segment - and the jurisdiction affiliations.
