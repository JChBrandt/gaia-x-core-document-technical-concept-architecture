## Service Composition

Service Composition is the ability for a [Service Offering](#service-offering) to describe the required presence of functional dependencies.

A functional dependency exposes behaviors related to external actions, which match its requirements and characteristics.

In the Gaia-X conceptual model, a [Service Offering](#service-offering)'s functional dependencies can include [Resources](#resource), or other [Service Offerings](#service-offering).

<!-- __Exemple 1__: A unicast service needs Address Resolution Protocol (ARP) support from the network's layer 2.  -->
__Exemple__: A high-availability web server which needs a reverse proxy and two web servers.

```mermaid
erDiagram
    HAWebService ||--|| ReverseProxy : depends_on
    HAWebService ||--|{ WebService : depends_on
    HAWebService ||..o| DomainName : depends_on
```
