## Catalogue

A Catalogue presents a list of available [Service Offerings](#service-offering). Catalogues are the main building blocks for the publication and discovery of [Self-Descriptions](#self-description) for Service Offerings by the [Participants](#participant).

### alias
- Gaia-X Catalogue
