## Consumer

A Consumer is a [Participant](#participant) who consumes a [Service Instance](#service-instance) in the Gaia-X ecosystem to enable digital offerings for [End-Users](#end-user).

Note: A Gaia-X Consumer will act as a Cloud Service Customer (CSC) of the relevant [Provider](#provider), but will probably also be offering cloud and/or edge services and thus acting as a Cloud Service Provider (CSP) in their own right to the customers and partners of their own business. The latter are considered [End-Users](#end-user) from a Gaia-X perspective. 
