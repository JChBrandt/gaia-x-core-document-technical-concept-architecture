# Gaia-X Operating Model

Gaia-X in its unique endeavour must have an operating model enabling a widespread adoption by small and medium-sized enterprises up to large organisations, including those in highly regulated markets, to be sustainable and scalable.

To achieve the objectives above, a non-exhaustive list of Critical Success Factors (CSFs) includes these points:

1. The operating model must provide clear and unambiguous added value to all Participants
2. The operating model must have a transparent governance and trust model with identified accountability and liability, that is clearly and fully explained to all Participants
3. The operating model must be easy to use by all Participants
4. The operating model must be financially sustainable for the Gaia-X Ecosystem
5. The operating model must be environmentally sustainable.

The first part of this chapter introduces the Gaia-X Ecosystem(s), as well as Trust Anchors.  Trust Anchors are defined, including details about who defines them and how they will be nominated.  

The second part defines Gaia-X Compliance, and how to become compliant. It introduces the Gaia-X Compliance Service as well as the usage of Gaia-X Labels.

Finally, the last section will cover the Gaia-X Self-Descriptions life-cycle and the Gaia-X Registry, which provides essential support for the Gaia-X Decentralized Autonomous Ecosystem.

## Gaia-X Ecosystem and Ecosystems

Gaia-X Participants may want to simultaneously provide and/or consume certain services with the greatest number of other Participants and provide and/or consume other types of services with a restricted set of Participants under custom policy rules. Furthermore, Gaia-X Participants may want to provide and/or consume certain services across ecosystems, i.e. within the Gaia-X Ecosystem. This operating model defines one global ecosystem with the possibility for Participants to create further ecosystems.

The main reasons to create an ecosystem are a combination of:

- maintenance of Self-Descriptions that are private to an Ecosystem
- usage of customs Gaia-X Compliant Trust Anchors

| Gaia-X Ecosystem                                                                  | Ecosystems                                                                                                 |
|-----------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------|
| Self-Descriptions are public                                                 | Self-Descriptions can be kept private                                                                 |
| Gaia-X Participants shall use Gaia-X compliant Trust Anchors                      | Ecosystem's Participants can use their own Trust Anchors                                                   |
| Gaia-X Compliance on Self-Description is mandatory                                | Only Self-Descriptions using Gaia-X compliant Trust Anchors are eligible to be awarded as Gaia-X compliant |
| Service Offerings are eligible to Gaia-X Labels                                   | Only compliant Service Offering Self-Descriptions are eligible to Gaia-X Labels                            |
| Uses decentralized applications interoperable with the Gaia-X Federation Services | Uses compatible Gaia-X Federation Services                                                                 |

Examples of Ecosystems:

- [Catena-X](https://catena-x.net/) - Automotive ecosystem
- EONA-X - Mobility ecosystem
- THEMIS - Tourism ecosystem

:information_source: It must be noted that by default, all Service Offerings shall include a policy using [Open Digital Rights Language](https://www.w3.org/TR/odrl-model/) (ODRL) to describe Permissions, Requirements and Constraints.
**These rules enable Providers, Federators, Consumers to express constraints, filtered out by requirements and to enforce permissions without needing to create an ecosystem with their own Trust Anchors.**

## Trust Anchors

For a given ecosystem, the Trust anchors are the entities considered by all Participants to be trustworthy when establishing the chain of cryptographic certificates.  
Ecosystems can select their own Trust Anchors, however, cross ecosystem trust requires the selected Trust Anchors to comply at least with the same rules that the common Gaia-X ecosystem Trust Anchors shall comply with.

The Gaia-X Association defines:

- the sets of rules to define the Trust Anchors:
    - [Trust Service Providers](https://en.wikipedia.org/wiki/Trust_service_provider).
    - Gaia-X Label Issuers
    - Trusted data source for Gaia-X Compliance
- the format of the Self-Descriptions and their compliance rules
- the Gaia-X Labels rulebook.

```mermaid
flowchart TB
    gx((Gaia-X<br>Association))
    part((Participants))
    label[Gaia-X Labels]
    sd[Self-Description Files]

    subgraph ta[Trust Anchors]
    end
    gx -- defines compliance rules for --> ta & sd

    gx -- defines --> label
    part -- nominate --> ta
```

The Trust Anchors are nominated by the Participants. The validation of the nominees is done automatically by validating the rules defined by the Gaia-X Association and supervised by the Gaia-X Association.

In turn, the Trust Anchors are used by the Participants to operate the Ecosystem(s).

```mermaid
flowchart TB
    subgraph ta[Trust Anchors]
        cab{{Gaia-X Label Issuers}}
        tsp{{Trust Service Providers}}
        tds{{Trusted Data Sources<br>for Gaia-X compliance}}
    end

    cab -- issues --> label[Gaia-X Labels]
    tsp -- proves --> id["Principal#commat;Participant's identity"]
    tds -- validates --> attr[Self-Description's attribute]
```

:information_source: Identity Providers are needed to verify a company's status and the mandate of the physical person over a company. A set of private/public keys will be issued to the company's representative and the public keys can be shared via a resolvable entry, like [DKIM](https://en.wikipedia.org/wiki/DomainKeys_Identified_Mail), [DID:DNS](https://datatracker.ietf.org/doc/html/draft-mayrhofer-did-dns-05) or [DID:WEB](https://w3c-ccg.github.io/did-method-web/), combined with the use of [Verifiable Credentials](https://www.w3.org/TR/vc-data-model/), to create the links between the identities and the services URLs.  
:information_source: Note:  Conformance Assessment Bodies (CABs) are just a special type of Provider, providing assessment services, and are therefore considered Participants.  Therefore, "Participants issuing VCs for other Participants" includes "CAB issuing VC to Providers".

## Ecosystem Launching Phase

To kickstart the Gaia-X ecosystem during the initial months, the Gaia-X Association will nominate itself as a Gaia-X Label Issuer and Trust Service Provider in order to showcase and validate the operating model. This is only a short-term, temporary situation.  
For the European Participants, the [National and EU eIDAS Trusted Lists](https://webgate.ec.europa.eu/tl-browser/#/) of _Qualified Certificate for Electronic Signature_ are considered to be Gaia-X compliant Trust Service Providers.

## Gaia-X Compliance

The Gaia-X compliance is defined as the process of going through and validating the set of automatically enforceable rules to achieve the minimum level of Self-Description compatibility in terms of:

- serialization format and syntax.
- cryptographic signature validation.
- attribute value consistency.
- attribute value verification.

Whenever possible, the verification of Self-Descriptions' attribute values is done either by using publicly available open data, and performing tests or using data from Trusted Data Sources as defined in the previous section.  These catalogues can then be referred to as Trusted Gaia-X Catalogues. 

Gaia-X Compliance has been referred before as _Regulation by Automation._

:information_source: It is important to note that this compliance is independent of the Self-Description's attribute validation done by the Issuer as defined in the [W3C Verifiable Credential](https://www.w3.org/TR/vc-data-model/#issuer).

The set of rules is versioned and will evolve over time to adapt to legal and market requirements.  

**One of the first Gaia-X added values is the creation of a [FAIR](https://www.go-fair.org/fair-principles/) (findable, accessible,interoperable, reusable) knowledge graph of verifiable and composable Self-Descriptions.**  

The rules will be implemented using open-source code and a service instance of that source code is called a Gaia-X Compliance Service.

:information_source: For ecosystems, and to keep their Self-Descriptions private, the Gaia-X Compliance Service could be embedded, as a software library, into local Ecosystem catalogues. The catalogues able to provide a code attestation demonstrating the execution of the unmodified source code are referred as Trusted Gaia-X Catalogues.

### Initial Set of Rules

Below is a non-exhaustive list of automatic checks to be performed:

- **serialization format and syntax**.
    - The Self-Description must parse as a well-formed JSON-LD. The RDF graph defined by this serialization must validate against the [SHACL](https://www.w3.org/TR/shacl/) shapes defined by Gaia-X.
    - All European Providers shall specify their company [ISO 6523](https://en.wikipedia.org/wiki/ISO/IEC_6523) EUID as specified in the section 8 of the [Commission Implementing Regulation (EU) 2015/884](https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=uriserv:OJ.L_.2015.144.01.0001.01.ENG).
    - ...
- **cryptographic signature validation**.
    - The Service Offering Provider identity must be verified by one of the Gaia-X approved Trust Service Providers.
    - All Self-Description's mandatory attribute signatures must have at least one of the Trust Anchor as root Certificate Authority.
    - ...
- **attribute value consistency**.
    - The value of the `service.jurisdiction` attributes must be consistent with the `service.provided_by.location` attributes.
    - The value of the `service.jurisdiction` attributes must be consistent with the _Governing Law_ from the Terms&Conditions document `service.terms_and_conditions` attributes.
    - ...
- **attribute value verification**.
    - If declared, the value of GDPR compliance must come from one of the [European Data Protection Board - EDPB](https://edpb.europa.eu/edpb_en) approved Code of Conducts, such as [CISPE](https://cispe.cloud/) or [EUCloudCOC](https://eucoc.cloud/en/home/).
    - If declared, the value of [European Cybersecurity Certification Scheme for Cloud Services](https://www.enisa.europa.eu/publications/eucs-cloud-service-scheme) compliance must come from one of the [ENISA](https://www.enisa.europa.eu/) recognized scheme such as [C5](https://www.bsi.bund.de/EN/Topics/CloudComputing/Compliance_Criteria_Catalogue/Compliance_Criteria_Catalogue_node.html) or [SecNumCloud](https://www.ssi.gouv.fr/entreprise/qualifications/prestataires-de-services-de-confiance-qualifies/prestataires-de-service-dinformatique-en-nuage-secnumcloud/).
    - ...

### Usage of data from Trusted Data Sources

It is expected that checking the validity of Self-Descriptions using data will introduce costs. In the context of the main Gaia-X Ecosystem, a proposal to cover the operating cost is described later in this document with the introduction of a [Gaia-X Decentralized Autonomous Ecosystem](#gaia-x-decentralized-autonomous-ecosystem).

:information_source: Other ecosystems are autonomous and this operating model doesn't cover how the operating cost of ecosystems should be handled.

Example of potential data Trusted Sources: [CISPE](https://cispe.cloud/), [Cloud Mercato](https://pcr.cloud-mercato.com/), _to be completed with a call for action from Gaia-X Association members_

## Gaia-X Labels

From the [European Data Governance Act](https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:52020PC0767) proposal:

> As a compulsory scheme this could generate higher costs, which could potentially have a prohibitive impact on SMEs and startups, and the market is not mature enough for a compulsory certification scheme; therefore, lower intensity regulatory intervention was identified as the preferred policy option.  
> However, the higher intensity regulatory intervention in the form of a compulsory scheme was also identified as a feasible alternative, as it would bring significantly higher trust to the functioning of data intermediaries, and would establish clear rules for how these intermediaries are supposed to act in the European data market.

The decision for the Gaia-X Association is to adopt a compulsory scheme for Gaia-X compliance - see previous section - and an optional scheme for Gaia-X Labels, to ensure a common level of transparency and interoperability while limiting the regulatory burden on the market players.

Labels are issued for Service Offerings only and are the result of the combination of several Self-Description compliant attributes, that individually would be insufficient to support business or regulatory decisions.  
The issued Labels must include a version number to allow continuous evolution of the set of rules and the precise set of rules in a “rulebook” defined by the Gaia-X Association, which must include a workflow for compliance re-validation.

From a technical point of view, a Label is a [W3C Verifiable Credential](https://www.w3.org/TR/vc-data-model/#proofs-signatures), similar to Self-descriptions' attributes credentials that are described in the next section.

The management of the rulebook and its governance is described in the Gaia-X Labels document expected in October 2021.

## Gaia-X Self-Description

Gaia-X Self-Descriptions describe in a machine interpretable format any of the entities of the Gaia-X Conceptual Model.

```mermaid
flowchart LR
    entity[Entity]
    sd[Self-Descriptions]
    schema[Schema]
    entity -- described by --> sd
    sd -- validated against --> schema
```

It means that there are Self-Descriptions for all Participant's Roles: `Consumer`, `Federator`, `Provider` and all the other entities in an Ecosystem's scope such as `Resource` and `Service Offering`.

Each Gaia-X entity makes [Claim](https://www.w3.org/TR/vc-data-model/#claims)s, which are validated and signed by 3rd parties. Those signed Claims are defined as [Verifiable Credentials](https://www.w3.org/TR/vc-data-model/#credentials) and presented by the entity as [Verifiable Presentations](https://www.w3.org/TR/vc-data-model/#presentations).  

Technically speaking, Self-Descriptions are [W3C Verifiable Presentations](https://www.w3.org/TR/vc-data-model/#presentations) in the [JSON-LD serialization of the RDF graph data model](https://www.w3.org/2018/jsonld-cg-reports/json-ld/#serializing-deserializing-rdf).

The following workflow describes how Gaia-X Self-Descriptions are created following the vocabulary of the [W3C Verifiable Credentials Data Model](https://www.w3.org/TR/vc-data-model/#ecosystem-overview) standard.


| W3C Term                                                                      | Example with a car                                                          |
|-------------------------------------------------------------------------------|-----------------------------------------------------------------------------|
| [Claim](https://www.w3.org/TR/vc-data-model/#claims)                          | My car is red                                                               |
| [Verifiable Credential](https://www.w3.org/TR/vc-data-model/#credentials)     | The garage's attestation that my car is red                             |
| [Verifiable Presentation](https://www.w3.org/TR/vc-data-model/#presentations) | Me showing to my friend the garage's attestation that my car is red |
| Issuer                                                                        | The garage                                                                  |
| Holder                                                                        | Myself                                                                      |
| Verifier                                                                      | My friends                                                                  |

```mermaid
sequenceDiagram
    autonumber
    participant part as Participant - Holder
    participant val as Participant - Issuer<br>or<br>Gaia-X Label Issuer

    activate part
        part ->> part: creates Claims and self-signs them
        loop for each Claim
        opt when possible
            part ->> val: requests verification.
            Note over val: performs automated and/or manual checks.
            val ->> part : issues Verifiable Credential (VC) as proof.
        end
        end
    deactivate part
    part ->> part: compiles all VCs into a Verifiable Presentation (VP).
```

### Difference between Self-Description's proofs (VC), Gaia-X Compliance and Gaia-X Labels.

The Verifiable Credentials are issued by other Participants, including Conformity Assessment Bodies. Verifiable Credentials can also be used to build a reputation system in the knowledge graph.

The Gaia-X Compliance insures that the required level of information for the users to take educated decisions is available and the information is verified or verifiable.

The Gaia-X Labels set thresholds for specific industries, markets or regulated activities.

|                          | Attribute's Verifiable Credentials | Gaia-X Compliance          | Gaia-X Labels              |
|--------------------------|------------------------------------|----------------------------|----------------------------|
| Technical implementation | W3C Verifiable Credentials         | W3C Verifiable Credentials | W3C Verifiable Credentials |
| Credential Issuer        | Any Gaia-X Participant             | Gaia-X Compliance Service  | Gaia-X Label Issuer        |
| Application scope        | All Self-Descriptions              | All Self-Descriptions      | Service Offerings          |
| Assessment method(s)     | Manual or Automated                | Fully automated            | Manual or Automated        |
| Issuance's temporality   | Frequent updates                   | Frequent updates           | Slow updates (~yearly)     |

A attribute's Verifiable Credential is Gaia-X conformant if the Issuer of the Verifiable Credential has itself an identity coming from one of the Trust Anchors.

```mermaid
flowchart LR
    part["Participants"]

    subgraph ta[Trust Anchors]
        tsp{{Trust Service Provider}}
    end

    subgraph sd[Self-Descriptions]
        cred[Credentials]
    end

    tsp -- proves identity of --> part

    part -- hold --> sd
    part -- issue --> cred
    part -- verify --> cred
```

A Label is Gaia-X conformant if the Issuer of the Credential is one of the Trust Anchors' Gaia-X Label Issuers.

```mermaid
flowchart LR
    part["Participants"]

    subgraph ta[Trust Anchors]
        cab{{Gaia-X Label Issuers}}
    end

    subgraph sd[Self-Descriptions]
        cred["Gaia-X Labels<br>&#40;Credentials&#41;"]
    end

    part -- hold --> sd
    cab -- issue --> cred
    part -- verify --> cred
```

### Self-Description compliance

A Self-Description qualified as Gaia-X compliant must be submitted to a Gaia-X Compliance Service instance as defined in the section above.

The result of that submission is captured in two ways:

- as a Verifiable Credential: If the compliance is validated, the Gaia-X Compliance Service issues a Verifiable Credential that can later be inserted into the Self-Description. This method aligns with the self sovereign principle of the holder being in charge of the information.
- If the Gaia-X Compliance Service is called via the Gaia-X Registry, the Gaia-X Registry will emit an event to synchronize Catalogues. The event contains the URL the of the Self-Description file. The Gaia-X Registry is defined in the next section.

```mermaid
sequenceDiagram
    autonumber
    participant part as Participant
    participant comply as Gaia-X Compliance Service
    participant registry as Gaia-X Registry
   
    Note over part: decides where to store the Self-Description files.

    part ->> comply: call service to validate Self-Description.
    activate comply
rect rgba(255, 128, 0, .1)
    comply -) comply: performs Gaia-X compliance check.
end
    comply ->> part: issue a Verifiable Credential
    opt if called via the Gaia-X Registry
    comply -) registry: emit events with URLs<br>of validated Self-Description.
    end
    deactivate comply
```

### Self-Description Remediation

Self-Descriptions may become invalid over time. The Chapter *Federated Catalogue* section *Self-Description* describes three states declaring a Self-Description as invalid:

- End-of-Life (after a timeout date, e.g., the expiry of a cryptographic signature)
- Deprecated (replaced by a newer Self-Description)
- Revoked (by the original issuer or a trusted party, e.g., because it contained incorrect or fraudulent information)

End-of-Life and Deprecated can be deduced automatically based in the information already stored in the Gaia-X Registry or Gaia-X Catalogues. There are no additional processes to define. This section describes how Self-Descriptions are revoked. 

The importance of Gaia-X compliance will grow over time, covering more and more Gaia-X principles such as interoperability, portability, and security. However, automation alone is not enough and the operating model must include a mechanism to demotivate malicious actors to corrupt the Registry and Catalogues.

The revocation of Self-Descriptions can be done in various ways:

- **Revocation or Deprecation by authorship**: The author of a Self-Description revokes or deprecates the Self-Description explicitly.
- **Revocation by automation**: The Gaia-X Compliance Service found at least one self-described attribute not validating the compliance rules.
- **Suspension and Revocation by manual decision**: After an audit by a compliant Gaia-X Participant, if at least one self-described attribute is found to be incorrect, the suspension of the Self-Descriptions is automatic. The revocation is submitted for approval to the Gaia-X Association with the opportunity for the Self-Description's owner to state its views in a matter of days. To minimize subjective decisions and promote transparency, the voting results will be visible and stored on the Gaia-X Registry or in local Ecosystem's Registry.
<!-- - Provider found to be convicted by the Court of Justice of the European Union - CJEU - or a European's Member State court of justice on Data Breach will lead to the revocation of all Provider's Services Offerings's Labels. -->

## Gaia-X Decentralized Autonomous Ecosystem

The operating model described in this chapter motivates the creation of a Gaia-X decentralized autonomous Ecosystem following the principles of a Decentralized Autonomous Organisation[^dao], with the following characteristics:

- Compliance is achieved through a set of automatically enforceable rules whose goal is to incentivize its community members to achieve a shared common mission.
- Maximizing the decentralization at all levels to reduce lock-in and lock-out effects.
- Minimizing the governance and central leadership to minimize liability exposure and regulatory capture.
- The ecosystem has its own rules, including management of its own funds.
- The ecosystem is operated by the ecosystem's Participants

[^dao]: Example of the setup of a DAO <https://blockchainhub.net/dao-decentralized-autonomous-organization/>

:information_source: Other ecosystems are autonomous and this operating model does not enforce how internal ecosystem governance should be handled.

### Gaia-X Registry

The Gaia-X Registry is a public distributed, non-reputable, immutable, permissionless database with a decentralized infrastructure and the capacity to automate code execution.

:information_source: The Ecosystems may want to have their own instance of a local Gaia-X Registry or equivalent. Technically, this component can be part of the ecosystem local Catalogues.

The Gaia-X Registry is the backbone of the ecosystem governance which stores information, similarly to the [Official Journal of the European Union](https://eur-lex.europa.eu/oj/direct-access.html), such as:

- the nominations of the Trust Anchors
- the result of the Trust Anchors validation processes.
- the potential revocation of Trust Anchors identity.
- the vote and results of the Gaia-X Association roll call vote, similar to the rules of the [plenary of the European Parliament](https://www.europarl.europa.eu/about-parliament/en/organisation-and-rules/how-plenary-works)
- the URLs of the Self-Description Schemas defined by Gaia-X
- the URLs of Catalogue's Self-Descriptions
- ...

It also facilitates the provision of:

1. A decentralized network with smart contract functionality.
2. Voting mechanisms that ensure integrity, non-repudiation and confidentiality.
3. Access to a Gaia-X Compliance Service instance.
4. A fully operational, decentralized and easily searchable catalogue[^OP].
5. A list of Participants' identities and Self-Description URIs which violate Gaia-X membership rules. This list must be used by all Gaia-X Trusted Catalogue providers to filter out any inappropriate content.
6. Tokens to cover the operating cost of the Gaia-X Ecosystem. This specific point can be abstracted by 3rd party brokers wrapping token usage with fiat currency, providing opportunities for new services to be created by the Participants. Emitting tokens for the Gaia-X Association's members is also considered. A first version of the Gaia-X Business Model will be released in Q4 2021.

:information_source: Each entry in the Gaia-X Registry is considered as a transaction. A transaction contains [DID](https://w3c.github.io/did-core/)s of all actors involved in the transaction and metadata about the transaction in a machine readable format.  The basic rule for a transaction to be valid is that all DIDs have one of the Trust Anchors as root Certificate Authorities.  Please also note that the Registry stores all revoked Trust Anchors.

This model enables the Participants to operate in the Gaia-X ecosystem, to autonomously register information, and to access the information which is verifiable by other Participants.  

[^OP]: Example of decentralized data and algorithms marketplace <https://oceanprotocol.com/>
### Ecosystem launching phase

In order to enable the 1st scenario which is:

> As a Gaia-X Provider, I want to publish the self-description of my Service Offerings and I want my Service Offerings to be made available to all Ecosystems.

and until the inter-catalogue synchronization is documented, the Registry will also be used to store, directly or indirectly via an external storage, the Self-Descriptions' URLs.

### Verifiable Presentation Verification

The Gaia-X Registry, or a private one, independently of its implementation, is the single source of truth for the Ecosystem.  
It allows any `Participant` to verify the validity of signatures.

```mermaid
sequenceDiagram
    autonumber
    participant cat as Catalogue
    participant user as Consumer
    participant registry as Gaia-X Registry
    user ->> cat: searches for services<br>by requirements.
    cat ->> user: returns results.
rect rgba(128, 128, 0, .1)
    Note left of  registry: Those checks can also be done by the<br>catalogues to provide a more user-friendly<br>experience for non expert users.
    loop for each Provider
        user ->> registry: can verify if the Provider's identity is from<br>a Gaia-X compliant Trust Anchor.
    end

    loop for each Claim
        user ->> registry: can verify if the VC Issuer's identity is from<br>a Gaia-X compliant Trust Anchor.
    end

    loop for each Label
        user ->> registry: can verify if the Labels are from<br>a Gaia-X compliant Trust Anchor.
    end

    loop for each signature
        user ->> registry: can verify if any signatures were revoked.
    end
end
```
*Example with `Consumer` and main `Gaia-X Registry`*

<!-- ### Data curation

By offering transparent access to the Gaia-X registry structured and verifiable Service-Descriptions, plus visibility on Service Instance consumption, the Participants can extrapolate about the data quality.

Other metadata, such as using [Great Expectations](https://github.com/great-expectations/great_expectations) can be enforced at the Data interoperability layer to promote a Data quality score. -->
