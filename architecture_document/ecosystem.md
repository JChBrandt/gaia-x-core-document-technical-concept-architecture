# Gaia-X Ecosystems

## Gaia-X as Enabler for Ecosystems

The Gaia-X Architecture enables Ecosystems and data spaces using the
elements explained in the [Gaia-X Conceptual Model](conceptual_model.md) in general
and the [Federation Services](conceptual_model.md#federation-services) in particular.

An Ecosystem is an organizing principle describing the interaction of
different actors and their environment as an integrated whole, like in a
biological Ecosystem. In a technical context, it refers to a set of
loosely coupled actors who jointly create an economic community and its associated benefits.

Gaia-X proposes to structure a Data Ecosystem and an
Infrastructure Ecosystem, each with a different focus on exchanged goods
and services. Despite each of them having a separate focus, they cannot be
viewed separately as they build upon each other, i.e. they are complementary.

The Gaia-X Ecosystem consists of the entirety of all individual
Ecosystems that use the Architecture and conform to Gaia-X requirements.
Several individual Ecosystems may exist (e.g., Catena-X) that orchestrate
themselves, use the Architecture and may or may not use the Federation
Services open source software.

![](figures/architecture-overview.png)
*Gaia-X Ecosystem Visualization*

The basic roles of Consumer and Provider are visualized as different
squares, while the Federator appears as a connecting layer, offering diverse core 
Federation Services.  Federation Services provide connections between and among the different elements as
well as between or among the different Ecosystems. The star-shaped element
visualizes that Consumers can act also as Providers by offering composed
services or processed data via Catalogues. Governance includes 
the Policy Rules, which are statements of objectives, rules, practices
or regulations governing the activities of Participants within the
Ecosystem. Additionally, the Architecture of Standards defines a target
for Gaia-X by analysing and integrating already existing standards for
data, sovereignty and infrastructure components.

## The Role of Federation Services for Ecosystems

The following figure visualizes how Federation Services Instances
are related to the Federator described in the conceptual model (see
section [Federator](conceptual_model.md#federator)). The Federators enable Federation Services by
obliging Federation Service Providers to provide concrete
Federation Service Instances. The sum of all Federation Service
Instances form the Federation Services.

![](figures/image12.png)
*Federation Services Relations*

### Goals of Federation Services

Federation Services aim to enable and facilitate interoperability and
portability of Resources within and across Gaia-X-based Ecosystems and
to provide Data Sovereignty. They ensure trust between or among Participants,
make Resources searchable, discoverable and consumable, and
provide means for Data Sovereignty in a distributed Ecosystem
environment.

They do not interfere with the business models of other members in the
Gaia-X Ecosystem, especially Providers and Consumers. Federation
Services are centrally defined while being federated themselves, so that
they are set up in a federated manner. In this way, they can be used
within individual Ecosystems and communities and, through their
federation, enable the sharing of data and services across Ecosystems or
communities as well as enable the interoperability and portability of data. The
set of Ecosystems that use the Federation Services form the Ecosystem.

### Nesting and Cascading of Federation Services

Federation Services can be nested and cascaded. Cascading is needed, for example, to ensure uniqueness of identities and Catalogue entries across different individual Ecosystems / communities that use Federation Services. (Comparable to DNS servers: there are local servers, but information can be pushed up to the root servers).

Therefore, a decentralised synchronization mechanism is necessary.

### Ecosystem Governance vs. Management Operations

To enable interoperability, portability and Data Sovereignty across
different Ecosystems and communities, Federation Services need to adhere
to common standards. These standards (e.g., related to service
Self-Description, digital identities, logging of data sharing
transactions, etc.) must be unambiguous and are therefore defined by the
Gaia-X Association AISBL. The Gaia-X Association AISBL owns the Compliance Framework and related
regulations or governance aspects. Different entities may take on the
role of Federator and Federation Services Provider.

#### Avoiding Silos

There may be Ecosystems that use the open source Federation Services but do not go through the Compliance and testing required by the Gaia-X Association AISBL. This does not affect the functionality of the Federation Services within specific Ecosystems but would hinder their interaction.

To enable open Ecosystems and avoid "siloed" use of Federation Services, only those that are compliant, interoperable (and tested) are designated as Ecosystems. Therefore, the Federation Services act as a connecting element not only between different Participants, commodities, but also Ecosystems (see above).

The following table presents how the Federation Services contribute to
the Architecture Requirements that are mentioned in section [Architecture Requirements](overview.md#architecture-requirements).


| Requirement                       | Relation to the Federation Services |
|-----------------------------------|-------------------------------------|
| Interoperability    | <ul><li>The Federated Catalogues ensure that Providers offer services through the whole technology stack. The common Self-Description scheme also enables interoperability.</li><li>A shared Compliance Framework and the use of existing standards supports the combination and interaction between different Resources.</li><li>The Identity and Trust mechanisms enable unique identification in a federated, distributed setting.</li><li>The possibility to exchange data with full control and enforcement of policies as well as logging options encourages Participants to do so. Semantic interoperability enables that data exchange.</li></ul> |
| Portability         | <ul><li>The Federated Catalogues encourage Providers to offer Resources with transparent Self-Descriptions and make it possible to find the right kind of service that is "fit for purpose" and makes the interaction possible.</li><li>The open source implementations of the Federation Services provide a common technical basis and enables movement of Resources in ecosystems and across different ecosystems.</li><li>Common compliance levels and the re-use of existing standards supports portability of data and services.</li></ul> |
| Sovereignty         | <ul><li>Identity and Trust provide the foundation for privacy considerations as well as access and usage rights. Standards for sovereign data exchange enable logging functions and Usage Policies. The Self-Descriptions offer the opportunity to specify and attach Usage Policies for Data Resources.</li></ul>|
| Security and Trust  | <ul><li>The Architecture and Federation Services provide definitions for trust mechanisms that can be enabled by different entities and enable transparency.</li><li>Sovereign Data Exchange, as well as Compliance concerns address security considerations. The identity and trust mechanisms provide the basis. The Federated Catalogues present Self-Descriptions and provide transparency over Service Offerings.</li></ul> |

*Federation Services match the Architecture Requirements*


### Infrastructure Ecosystem

The Infrastructure Ecosystem has a focus on computing, storage and Interconnection elements. In GAIA-X Ecosystem these elements are designated as Nodes, Interconnections and different Software Resources. They range from low-level services like bare metal computing 
up to highly sophisticated offerings, such as high-performance
computing. Interconnection Services ensure secure and performant data
exchange between the different Providers, Consumers and their services.
Gaia-X enables combinations of services that range across multiple
Providers of the Ecosystem. The Interconnection Services are also the key enabler for the composition of services offered by diverse and distributed providers, ensuring the performance of single-provider networks on a multi-provider "composed" network.

### Data Ecosystem

Gaia-X facilitates Data Spaces which present a virtual data integration
concept, where data are made available in a decentralised manner, for
example, to combine and share data of stored in different cloud storage backends. Data
Spaces form the foundation of Data Ecosystems. In general, Data
Ecosystems enable Participants to leverage data as a strategic resource
in an inter-organizational network without restrictions of a fixed
defined partner or central keystone companies. For data to realize its
full potential, it must be made available in cross-company,
cross-industry Ecosystems. Therefore, Data Ecosystems not only enable
significant data value chain improvements, but provide the technical means to enable Data
Sovereignty. Such sovereign data sharing addresses different layers and
enables a broad range of business models that would otherwise be impossible. 
Trust and control mechanisms encourage the acceleration of data sharing and proliferate the growth of Ecosystems.

### Federation, Distribution, Decentralization and Sharing

The principles of federation, distribution, decentralization and sharing
are emphasized in the Federation Services as they provide several
benefits for the Ecosystem:


| Principle        | Need for Gaia-X          | Implemented in Gaia-X Architecture        |
|------------------|--------------------------|-------------------------------------------|
| Decentralization | Decentralization will ensure Gaia-X is not controlled by the few and strengthens the participation of the many. It also adds key technological properties like redundancy, and therefore resilience against unavailability and exploitability. Different implementations of this architecture create a diverse Ecosystem that can reflect the respective requirements and strengths of its Participants.<br><br>(example: IP address assignment) | The role of Federators may be taken by diverse actors.<br><br>The open source Federation Services can be used and changed according to specific new requirements as long as they are compliant and tested. |
| Distribution     | Distribution fosters the usage of different Resources by different Providers spread over geographical locations.<br><br>(Example: Domain Name System) | Self-Description ensures that all Resources and Service Offerings are defined standardized ways, which enables them to be listed in a searchable Catalogue, each with a unique Identifier. Therefore, it facilitates the reuse and distribution of these components. |
| Federation       | Federation technically enables connections and a web of trust between and among different parties in the Ecosystem(s). It addresses the following challenges:<ul><li>Decentralized processing locations</li><li>Multiple actors and stakeholders</li><li>Multiple technology stacks</li><li>Special policy requirements or regulated markets</li></ul><br>(Example: Autonomous Systems) | Each system can interact with each other, e.g., the Catalogues could exchange information and the Identity remains unique. Furthermore, different Conformity Assessment Bodies may exist. |
| Sharing          | Sharing of the relevant services and components contributes to the Ecosystem development.<br><br>Sharing and reuse of Resources across the Gaia-X Ecosystem enables positive spillovers, leading to new and often unforeseen economic growth opportunities. | The Federated Catalogues enable the matching between Providers and Consumers. Sovereign Data Exchange lowers hurdles for data exchange and Ecosystem creation. |

*Summary of Federation Services as enabler*


By utilizing common specifications and standards, harmonized rules and
policies, Gaia-X is well aligned with specifications like NIST Cloud
Federation Reference Architecture[^25]:

-   Security and collaboration context are not owned by a single entity

-   Participants in the Gaia-X Association AISBL jointly agree upon the common goals
    and governance of the Gaia-X Association AISBL

-   Participants can selectively make some of their Resources
    discoverable and accessible by other Participants in compliance with
    Gaia-X

-   Providers can restrict their discovery and disclose certain
    information but could risk losing their Gaia-X compliance level

[^25]: Bohn, R. B., Lee, C. A., & Michel, M. (2020). The NIST Cloud Federation Reference Architecture: Special Publication (NIST SP) - 500-332. NIST Pubs. <https://doi.org/10.6028/NIST.SP.500-332>

## Interoperability and Portability for Infrastructure and Data

For the success of a Federated Ecosystem it is of importance that data,
services and the underlying infrastructure can interact seamlessly with each
other. Therefore, portability and interoperability are two key
requirements for the success of Gaia-X as they are the cornerstones for
a working platform and ensure a fully functional federated, multi-provider
environment.

Interoperability is defined as the ability of several systems or
services to exchange information and to use the exchanged information
mutually. Portability refers to the enablement of data transfer and
processing to increase the usefulness of data as a strategic resource.
For services, portability implies that they can be migrated from one
provider to another, while the migration should be possible without
significant changes and adaptations and have an equivalent QoS (Quality of Service). 

### Areas of Interoperability and Portability

+The Gaia-X Ecosystem includes a huge variety of Participants and Service Offerings. Therefore, interoperability needs to be ensured on different levels (Infrastructure as a Service [IaaS] , Platform as a Service [PaaS], Software as a Service [Saas], data resources, and others) by means of Service Composition.

Regarding interoperability of data, core elements to be identified in this endeavour are API specifications and best practices for semantic data descriptions. The use of semantic data interoperability is seen as a foundation to eventually create a clear mapping between domain-specific approaches based on a community process and open source efforts.

## Infrastructure and Interconnection

To best accommodate the wide variety of Service Offerings, the Gaia-X
Architecture is based on the notion of a sovereign and flexible
Interconnection of Infrastructure and Data Ecosystems, where data is
flexibly exchanged between and among many different Participants. Therefore,
Interconnection Services represent a dedicated category of Resources as described in
section [Gaia-X Conceptual Model](conceptual_model.md).

There is a strong need for Interconnection Services for the different Nodes in
Gaia-X. These support the federation of the Infrastructure Ecosystem,
which in turn is the foundation of the Data Ecosystem. Due to different 
needs of the Consumers and Providers as well as to highly 
heterogeneous architectures, diverse requirements arise for
those Interconnections.

### Role of Interconnection and Networking Services

The recent survey (08/2021) performed by the German market research company Research in Action showed that requirements on the network and interconnection vary in different sectors [^26].  On average 25% of the companies in Healthcare & Social Assistance, Manufacturing & Automotive and Travel/Transport/Logistics require a connection that is separated from the public Internet. Thus offering protection from hacking attacks and malicious third-parties, as well as ensuring the resilience and constant availability of the connection. Almost 30% of the Travel, Transport, and Logistics sector shows an interest in a high-performance, low-latency, redundant interconnection service to enable fast response times for business critical tasks.  

These and other sectors are present in GAIA-X Data Spaces and can not always rely on the best-effort public Internet and in some cases require dedicated / exclusive connections, which, as alternative to single-owner networks, could be created by the composition of resources offered by different providers. As a result, Gaia-X defines interconnection and networking services as one of the key resources for reaching its goals (refer to the [Glossary]). Consequently, and as explained in section [Provider Use Cases](usecase.md#provider-use-cases), the Federated Catalogue must be extended with the rich variety of networking and interconnection services, considering, for instance, functional and non-functional QoS (Quality of Service) requirements; portability requirements, etc.

[^26]: https://www.de-cix.net/en/about-de-cix/media/press-releases/new-de-cix-market-survey-confirms-no-slowdown-in-sight-digital-transformation-continues-at-pace-in-companies-in-germany-and-the-usa 

Currently, Gaia-X addresses the architectural needs for networking and
Interconnection services via three building blocks: (i) a Self-Description model, 
which describes Interconnection Ressources and mandatory attributes necessary to describe 
interconnection & networking services [Appendix](appendix.md); (ii) Quality of Service assesment by for example inter-Node measurements, describing connection SLA indicators (guaranteed bandwidth, availability, latency, etc. ) between or among Gaia-X Participants; (iii) interoperable interconnection and networking services from different Participants based on e.g Internet, L2 point-to-point connection, private interconnection, etc. (refers to Network Service Composition)
In the current release we have mainly adressed (i) and (iii) that are explained below. 

### Interconnection Resource Mandatory Self-descriptions

In this sub-section we focus mainly on the
Self-Description of Gaia-X Nodes, where Interconnection and networking services
are addressed via the definition of attributes. The first set of mandatory Self-Descriptions attributes
describing Interconnection Resource considers QoS (Quality of Service) functional 
parameters relevant for real-time data services, e.g., latency, bandwidth, availability, packet loss (refer to [Appendix](appendix.md) for an exhaustive list of attributes). In case the Provider is not able to guarantee certain level of QoS, the best-effort value should be used to desribe these attributes.

For the future releases non-functional requirements for supported services
must also be defined (whether mandatory or not). Therefore, Self-Description of
Interconnection and networking services will not only be limited to QoS but
also address quality of experience (QoE)-related attributes and consider
non-functional requirements, such as security and reliability. A
distinct and rich description of these functional and non-functional
requirements enables differentiating between the different Service
Offerings and helps to select the appropriate Interconnection and
networking services from the Gaia-X Catalogues.

### Network Service Composition

Interconnection and Networking services can be composed via
heterogeneous offerings from multiple Providers and technologies. To
achieve flexibility but also sovereignty and trust, network service
composition shall be supported. It is also relevant to consider the
capability to describe Interconnection and networking services in a
flexible way. Such a composition must take existing approaches into
consideration and must be as rich as, e.g., composing a slice for
verticals, via private and public Clouds[^27].

[^27]: For a comprehensive view of the current discussion in the broader Gaia-X community, extra documents from the open working packages can be found on the Gaia-X community platform at <https://gaia.coyocloud.com/web/public-link/e01b9066-3823-42a7-b10b-9596871059ef/download>.

A network service composition framework embeds both functional and
non-functional requirements and has the capability to integrate metadata
(e.g., in the form of intents) to consider abstract descriptions of the
networking service components with their related requirements. Interface
definition languages need to be adopted to enable the composition of
functional elements to support network service composition. Furthermore,
taking the non-functional aspects for networking services into
consideration, the chosen interface definition languages have to be
coupled with data modelling languages. This supports the consideration
and integration of non-functional elements when composing network
services.

In addition to non-constraining interface definition languages and data
modelling languages, an overall networking service description framework
needs to be used. An example of available service description frameworks
that are relevant to consider by Gaia-X are, for instance, the OASIS
Topology and Orchestration Specification for Cloud Applications
(TOSCA)[^28]. With respect to network service management and
orchestration, potential candidates cover but are not limited to the ONF
Software Defined Network (SDN) architecture and the ETSI Standards for
Network Function Virtualization (ETSI NFV)[^29]. 

A crucial aspect to achieve an adequate network service composition is
to integrate support for the intertwining of networking services and
application level services. Thus, both semantic and syntactic
interoperability need to be ensured. Specifically, an adequate and
semantic support for the available and multiple communication protocols
is required. This relates to the OSI Layer 2 and 3 communication
aspects, but it has also to accommodate additional protocols. Each use
case has its own set of building blocks. Therefore, the Interconnection
services should cover diverse scenarios ranging from a single
point-to-point connection to complex multipoint architectures. 

Moreover there is a need for GAIA-X Compliant Network Service Composition Framework, talking to APIs (e.g, the open source IX-API[^30], Terraform[^31], etc.) of Providers (in this case Interconnection Service Providers),  as well as solutions from the area of
Software Defined Networking that can be used to flexibly interconnect and
configure these architectures, and consider host-reachability and
content-oriented developments.

[^28]: OASIS (2013). Topology and Orchestration Specification for Cloud Applications Version 1.0. <http://docs.oasis-open.org/tosca/TOSCA/v1.0/TOSCA-v1.0.html>
[^29]: ETSI. Network Functions Virtualisation (NFV). <https://www.etsi.org/technologies/nfv>
[^30]: IX-API. <https://ix-api.net/>
[^31]: Terraform. <https://www.terraform.io/> 

This framework should also cover all communication aspects in both public and private networks as well as across all OSI Layers from OSI Layer 4 to OSI Layer 1. In order to achieve that, relevant services need to be defined. To understand this concept an its relation to the defined mandatory attributes and Gaia-X Conceptual Model, let us consider the following scenario shown in Figure below.
 
![](figures/interc_example.png)
*General Use Case: Ordering of Interconnection Service*

A Gaia-X Consumer requests an interconnection service, providing information about the desired interconnection points (via Interconnection Service Offering Templates) from the GAIA-X Interconnection Service Provider. At this point in time, the interconnection service is instantiated (Interconnection Service Instance) and it relies on various Interconnection resources that are responsible for the actual connection establishment. These interconnection resources can be composed of different attributes (the very first mandatory set is found in the Appendix).

**First set of Elements for the composition of Interconnection&Networking services**

In the scope of the Gaia-X Ecosystem a Network (and/or Interconnection) Service can be a specific Gaia-X compatible Service Offering running on one or many Nodes. This type of Service provides network functions such as Switching, Routing, Security Functions, Load Balancing, etc.

Such a Service is provided by a Provider and shall be described by a Self-Description and available via Federated Catalogue for Consumers to deploy and use. 
To interconnect these or other Gaia-X Services, communication paths can flow via one or multiple Network Services. In any case, communication between Gaia-X Services (including communication between Network Services) would need to be established over one of the following interconnection resources:

*Networks*
 
Networks are logical communication resources which can directly bind to Nodes via a link to a Nodes Port.
A Network will link together at least two or more Ports of one or more Nodes. It is defined by the Network Protocol used, as defined in ISO/IEC standard 7498-1:[^32] for Layer 3 of the ISO/OSI Model and will use Network Addressing for the Nodes as appropriate for the Network Protocol.
An example of Network Definitions can be found in the TOSCA Framework.
[^32] <https://www.iso.org/obp/ui/#iso:std:iso-iec:7498:-1:ed-1:v2:en>
 
*Route Resources*

A Route Ressource Template defines the reachability of one or more Networks and can be subscribed individually by users. A Route Resource provides connectivity to other networks, nodes or services by using other Interconnection Ressources or Services. These ressources or services can be explicitly defined or be abstracted by the Route Ressource itself. 
A Route Ressource in most cases will provide access to multiple networks, nodes or services at once and hence has a cardinality of 1..\*
Generally, a Route Resource will only provide connectivity in one direction, while the route back needs to be established from the connected service. 

*Connection Resources*

Connection Resources provide a direct communication path between one or many Networks, Nodes or Services. They can use other Interconnection Resources. These services or resources can be explicitly defined or be abstracted by the Connection Resource. A Connection Resource will connect at least two Networks, Nodes or Services and hence has a cardinality of 2..\*
Connection Resources are generally provided as connections between two or more interconnection points which are defined in terms of location and jurisdiction.   

*Physical Medium Resource*

Physical Medium Resources are direct physical connections between exactly two Networks or Nodes and provide a direct communication path between single Networks or Nodes. They are defined by specific endpoints and cannot use other Interconnection Resources to realize the connection and thus are the atomic instances of interconnection services.  They have a cardinality of 2..2. 

*Service Orchestration*

To allow Customers to consume any of the above-mentioned Interconnection Ressources or compose new Services from them, each Ressource needs to be available as a Ressource Template from the GAIA-X Catalogs and needs a self-description. To enable Network and Interconnection Service Composition in an automated way, any Interconnection Service Request needs to be described from a functional and non-functional requirement view. 
For Interconnection Ressources and the services composed from those ressources, such as Routes, Connections or Physical Medium this can include network based quality parameters such as bandwidth, latency, jitter and availability.
To be able to compose Interconnection Services, Interconnection Ressource Templates need attributes that describe their dependency on other Interconnection Resources, so that Customers can query the Catalog for the dependencies and instantiate these  Resources according to the requirements.
In this sense, a Route Resource Template could potentially provide no quality attributes itself, but depend on a specific subset of Connection or Physical Medium Ressources that provide specific quality attributes. These attributes then are “inherited” by the Route Service composed from the Connection and Physical Medium Ressources and can be provided as a Service themselves. 

A Gaia-X compliant orchestration service will then compose the full Interconnection Service required by instantiating Resource Instances from the selected Resource Templates.

**Interconnection Platforms**

In order to ensure certain requirements of latency, bandwidth and security, Gaia-X has to be able to propose more than the classic Internet with the Best-Effort principle does. Specific Interconnection Resources such as Connection or Physical Medium Resources allow for those functional requirements to be met. 

As the resources and data from the Provider and Consumer use cases are located in different physical locations, namely in data centers that could be spread all over Europe it would result in a overly complex and redundant number of connections, if we would interconnect them directly, which would be expensive, insufficient and neither dynamic nor performant. In the example shown below, if we want to interconnect 8 locations or nodes we would require 28 connections (picture on the left). However, with the introduction of an Interconnection platform we would need only eight connections (picture on the right). This not only means that multi-cloud setups will become easier and faster, but also that dynamic service provisioning will be possible.

![](figures/Gaia-X_interc.png)
*Visualization of connections via Interconnection Platform: left - without platform, right - with platform*

The solution that we see is Gaia-X compatible Interconnection platforms with common or standardized APIs or Interfaces to a GAIA-X compliant orchestrator via which the interconnection Resources can be connected together.  Such an Interconnection Platform can operate as a specific Gaia-X Node and provide a high number of interconnection points at one location. This platform will be provided and operated by Gaia-X Providers (e.g., Interconnection Providers).

For those customers who do not want that their traffic passing via the platform, it will also be possible to create a Closed User Group (Network Resource as VPN) or to directly use private Connection or Physical Medium Resources (asan example).



