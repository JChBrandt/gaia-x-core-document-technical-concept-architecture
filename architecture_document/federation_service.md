# Federation Services

*Federation Services* are necessary to enable a Federation of
infrastructure and data, provided through an open source reference
implementation. This will open up technology wherever possible, while
existing *Certifications* and standards for *Accreditation* will be
recognized.

Details about the operationalization of *Federation Services* will be
outlined in the upcoming Federation Services documents. Details about
the role of *Federation Services* for *Ecosystems* are elaborated in the section
[Gaia-X Ecosystems](ecosystem.md), with an overview shown in the figure below.

-   The *[Federated Catalogue](#federated-catalogue)* constitutes an indexed
    repository of Gaia-X Self-Descriptions to enable the discovery and
    selection of Providers and their Service Offerings. The
    Self-Descriptions are the properties and Claims of
    Participants and Resources, representing key elements of transparency
    and trust in Gaia-X.

-   *[Identity and Trust](#identity-and-trust)* covers identification, authentication and
    authorization, credential management, decentralized Identity management
    as well as the verification of analogue credentials.

-   *[Data Sovereignty Services](#data-sovereignty-services)* enable the sovereign data
    exchange of Participants by providing a Data Agreement Service and a
    Data Logging Service to enable the enforcement of Policies. Furthermore,
    usage constraints for data exchange can be expressed by Provider
    Policies within the Self-Descriptions.

-   *[Compliance](#compliance)* includes mechanisms to ensure that
    Participants adhere to the Policy Rules in areas such as
    security, privacy, transparency and interoperability during
    onboarding and service delivery.

-   *[Gaia-X Portals and APIs](#gaia-x-portals-and-apis)* will support onboarding and
    Accreditation of Participants, demonstrate service discovery,
    orchestration and provisioning of sample services.

![](figures/image4.png)
*Gaia-X Federation Services and Portal as covered in the Architecture Document*

## Federated Catalogue

Self-Descriptions intended for public usage can be published in a Catalogue
where they can be found by potential Consumers. The goal of Catalogues is to
enable Consumers to find best-matching offerings and to monitor for relevant
changes of the offerings. The Providers decide in a self-sovereign manner which
information they want to make public in a Catalogue and which information they
only want to share privately.

A Catalogue stores Self-Descriptions both as standalone and as aggregated in a graph
data structure. The Self-Description Storage contains the raw published
Self-Description files in the JSON-LD format together with additional lifecycle
metadata. The Self-Description Graph imports the Self-Descriptions from the
Self-Description Storage into an aggregate data structure. The individual
Self-Descriptions can reference each other. The Self-Description Graph is the
basis for advanced query mechanisms that consider the references between and among Self-Descriptions.

The system of Federated Catalogues includes an initial stateless Self-Description
browser provided by the Gaia-X, European Association for Data and Cloud, AISBL. 
In addition, Ecosystem-specific Catalogues (e.g., for the healthcare
domain) and even company-internal Catalogues (with private
Self-Descriptions to be used only internally) can be linked to the system of federated Catalogues. The Catalogue federation is used to exchange relevant Self-Descriptions and updates thereof. It is not used to execute queries in a distributed fashion.

Cross-referencing is enabled by unique Identifiers as described in [Identity and Trust](federation_service.md#identity-and-trust).
While uniqueness means that Identifiers do not refer to more than one entity, 
there can be several Identifiers referring to the same entity. 
A Catalogue should not use multiple Identifiers for the same entity.

The system of Federated Catalogues consists of a top-level Catalogue
operated by Gaia-X European Association for Data and Cloud AISBL, and provides
the means to link to Ecosystem-specific Catalogues (e.g., for the healthcare
domain) and even company-internal Catalogues with private Self-Descriptions to be used only internally. Self-Descriptions in a
Catalogue are either loaded directly into a Catalogue or exchanged from
another Catalogue through inter-Catalogue synchronization functions.

Since Self-Descriptions are protected by cryptographic signatures, they
are immutable and cannot be changed once published. This implies that after any changes to a Self-Description, the Participant as the Self-Description issuer has to sign the Self-Description again and release it as a new version.
The lifecycle state of a Self-Description is described in additional metadata. 
There are four possible states for the Self-Description lifecycle. The default
state is "Active". The other states are terminal, i.e., no further state
transitions are allowed.  All states are listed below:

-   Active

-   End-of-Life (after a timeout date, e.g., the expiry of a
    cryptographic signature)

-   Deprecated (by a newer Self-Description)

-   Revoked (by the original issuer or a trusted party, e.g., because it
    contained wrong or fraudulent information)

The Catalogues provide access to the raw Self-Descriptions that are
currently loaded including the lifecycle metadata. This allows Consumers
to verify the Self-Descriptions and the cryptographic proofs contained
in them in a self-service manner.

The Self-Description Graph contains the information imported from the
Self-Descriptions that are known to a Catalogue and in an "Active"
lifecycle state. The Self-Description Graph allows for complex queries
across Self-Descriptions.

To present search results objectively and without discrimination,
compliant Catalogues use a query engine with no internal ranking of results.
Users can define filters and sort-criteria in their queries. But if some results have no unique
ordering according to the defined sort-criteria, they are randomized.
The random seed for the search result ordering is set on a per-session basis so that the
query results are repeatable within a session with a Catalogue.

Self-Descriptions intended for public usage can be published in a Catalogue
where they can be found by potential Consumers. The goal of Catalogues is to
enable Consumers to find best-matching offerings and to monitor for relevant
changes of the offerings. The Providers decide in a self-sovereign manner which
Self-Descriptions they want to share with a public Catalogue and which ones they
only want to share privately.
Options for private sharing include private Catalogues as well as channels external to Gaia-X,
such as encrypted email.

A Visitor is an anonymous user accessing a Catalogue without a known
account. Every Non-Visitor user (see Principal in section
3.2) interacts with a Catalogue REST API in the context of a session.
Another option to interact with a Catalogue is to use a GUI frontend
(e.g., a Gaia-X Portal or a custom GUI implementation) that uses a
Catalogue REST API in the background. The interaction between a
Catalogue and its GUI frontend is based on an authenticated session for
the individual user of the GUI frontend.

### Self-Description

Gaia-X Self-Descriptions express characteristics of Resources, Service Offerings and Participants that are linked to their respective
Identifiers. Providers are responsible for the creation of Self-Descriptions of their Resources. In addition to self-declared Claims made
by Participants about themselves or about the Service Offerings provided
by them, a Self-Description may comprise Credentials issued and signed
by trusted parties. Such Credentials include Claims about the Provider
or Resources, which have been asserted by the issuer.

Self-Descriptions in combination with trustworthy verification
mechanisms empower Participants in their decision-making processes.
Specifically, Self-Descriptions can be used for:

-   Discovery and composition of Service Offerings in a Catalogue

-   Tool-assisted evaluation, selection, integration and orchestration
    of Service Instances and Resources

-   Enforcement, continuous validation and trust monitoring together
    with Usage Policies

-   Negotiation of contractual terms concerning Resources of
    a Service Offering and Participants

Gaia-X Self-Descriptions are characterized by the following properties:

-   Machine-readable and machine-interpretable

-   Technology-agnostic

-   Adhering to a generalized schema with expressive semantics and
    validation rules

-   Interoperable, following standards in terms of format, structure,
    and included expressions (semantics)

-   Flexible, extensible and future-proof in that new properties can be
    easily added

-   Navigable and referenceable from anywhere in a unique, decentralized fashion

-   Accompanied by statements of proof (e.g., certificates and
    signatures), making them trustworthy by providing cryptographically
    secure verifiable information

The exchange format for Self-Descriptions is JSON-LD. JSON-LD uses JSON encoding to represent subject-predicate-object
triples according to the W3C Resource Description Framework (RDF).

A Self-Description contains the Identifier of the Asset, Resource or
Participant, metadata and one or more Credentials as shown in the figure below.
A Credential contains one or more Claims, comprised of subjects,
properties and values. The metadata of each Credential includes issuing
timestamps, expiry dates, issuer references and so forth. Each
Credential can have a cryptographic signature, wherein trusted parties
confirm the contained Claims. Claims may follow the same
subject-predicate-object structure of the data model. The W3C
Verifiable Credentials Data Model[^11] is the technical standard
to express Credentials and Claims using JSON-LD[^12].
When there exist multiple Credentials for the thing that is being self-described, e.g., Credentials issued and signed by Providers themselves, plus other Credentials such as Certifications provided by independent external bodies, they may be bundled into a Verifiable Presentation.  This most general case of a Self-Description is presented in the figure below.

[^11]: W3C. Verifiable Credentials Data Model 1.0: Expressing verifiable information on the Web [W3C Recommendation 19 November 2019]. <https://www.w3.org/TR/vc-data-model/>
[^12]: W3C. JSON-LD 1.1: A JSON-based Serialization for Linked Data [W3C Recommendation 16 July 2020]. <https://www.w3.org/TR/json-ld11/>

```mermaid
graph LR
subgraph Self-Description
    subgraph Verifiable Presentation
        metadata1[Metadata]
        vc1[Verifiable Credential - 1..*]
        proof1[Proof Info - 1..*]
    end

    subgraph Verifiable Credential
        metadata2[Metadata]
        claim[Claim - 1..*]
        proof2[Proof Info - 1..*]
    end

    vc1 -- 1 .. * --> claim
end
```
*Self-Description assembly model*

The generic data model for Claims is powerful and can be used to express
a large variety of statements. Individual Claims can be merged 
to express a graph of information about Resources (subjects). For
example, a Node complying with ISO 27001 is shown in the figure below.

```mermaid
graph TB
    subgraph Credential Graph
        cred123[Credential 123] -- credentialSubject --> node[Node]
        cred123 -- issuer --> duv[DUV]
        node -- hasCertificate --> iso(ISO 27001)
    end

    subgraph Proof Graph
        sig456[Signature 456] -- creator --> jane[Jane Doe]
        sig456 -- created --> date(2021-03-01 14:01:46)
    end

    duv -- proof ---> sig456

    %% classDef attribute fill:#f9f,stroke:#333,stroke-width:4px,rx:50%,ry:50%; %%
    classDef attribute stroke-width:2px,rx:50%,ry:50%;
    class iso,date attribute;
```
*Linked Claim statements as a graph representation*

The Self-Description of one entity may refer to another entity by its
Identifier. Identifiers in Gaia-X are URIs and follow the specification
of RFC 3986. While uniqueness means that Identifiers do not refer to more than
one entity, there can be several Identifiers referring to the same entity. A
Catalogue should not use multiple Identifiers for the same entity. 
Depending on the prefix of the URI, different technical systems are defined
to ensure uniqueness of Identifiers. For example, the use of a domain-name
as part of the Identifier, where only the owner of the domain-name shall
create Identifiers for it.

The relations between Self-Descriptions form a graph with typed edges, which is
called the Self-Description Graph. The Catalogues implement a query
algorithm on top of the Self-Description Graph. Furthermore,
Certification aspects and Usage Policies can be expressed and checked
based on the Self-Description Graph that cannot be gained from
individual Self-Descriptions. For example, a Consumer could use
Catalogue Services to require that a Service Instance cannot depend on
other Service Instances that are deployed on Nodes outside a
Consumer-specified list of acceptable countries.

To foster interoperability, Self-Description schemas with optional and
mandatory properties and relations are defined. A Self-Description has
to state which schemas are used in its metadata. Only properties and
relations defined in these schemas can be used. A Self-Description
schema corresponds to a class in RDF. The Self-Description schemas form
an extensible class hierarchy with inheritance of properties and
relations. Individual Gaia-X Ecosystems can extend the schema hierarchy
for their application domain.[^13] Such extensions must make an explicit
reference to the organization that is responsible for the development
and maintenance of the extension.

[^13]: This is analogous to how DCAT-AP specifies the application of DCAT for data portals in Europe; European Commission Semantic Interoperability Community. DCAT Application Profile for data portals in Europe. <https://joinup.ec.europa.eu/collection/semantic-interoperability-community-semic/solution/dcat-application-profile-data-portals-europe>

![](figures/schematic_inheritance_relations-Page-1.png)
*Schematic inheritance relations and properties for the top-level Self-Description*

The Self-Description Schemas can follow the Linked Data best
practices[^14] which makes the W3C Semantic Web family[^15] a possible
standard to be built upon to enable broad adoption and tooling.

[^14]: Berners-Lee, T. (2009). Linked Data. W3C. <https://www.w3.org/DesignIssues/LinkedData>
[^15]: W3C. (2015). Semantic Web. <https://www.w3.org/standards/semanticweb/>

Gaia-X aims at building upon existing schemas, preferably those that have
been standardized or at least widely adopted[^16] to get a common
understanding of the meaning and purpose of any property and Claim
statements. Examples of attribute categories per Self-Description in
Gaia-X are discussed in the Appendix [A1](appendix.md#a1),
<!--and a minimum set of mandatory attributes is presented in [A3](appendix.md#a3)-->.

[^16]: Examples include the W3C Organization Ontology (<https://www.w3.org/TR/vocab-org/>), the community-maintained schema.org vocabulary (<https://schema.org/>), the W3C Data Catalog Vocabulary DCAT (<https://www.w3.org/TR/vocab-dcat-2/>), the W3C Open Digital Rights Language (<https://www.w3.org/TR/odrl-model/>), and the International Data Spaces Information Model (<https://w3id.org/idsa/core>)

## Identity and Trust

Identities, which are used to gain access to the Ecosystem, rely on
unique Identifiers and a list of dependent attributes. Gaia-X uses existing Identities and does not maintain them directly. Uniqueness is ensured by
a specific Identifier format relying on properties of existing
protocols. The Identifiers are comparable in the raw form and should not 
contain more information than necessary (including Personal Identifiable Information). 
Trust - confidence in the Identity and capabilities of
Participants or Resources - is established by cryptographically
verifying Identities using the Federated Trust Model of Gaia-X, which is
a component that guarantees proof of identity of the involved
Participants to make sure that Gaia-X Participants are who they claim to
be. In the context of Identity and Trust, the digital representation of a natural person, acting on behalf of a Participant, is
referred to as a Principal. As Participants need to trust other
Participants and Service Offerings provided, it is important that the
Gaia-X Federated Trust Model provides transparency for everyone.
Therefore, proper lifecycle management is required, covering Identity
onboarding, maintaining, and offboarding. The table below shows the Participant
Lifecycle Process.

| Lifecycle Activity | Description |
|--------------------|-------------|
| Onboarding         | The accredited Conformity Assessment Bodies (CAB) of a Gaia-X Ecosystem, validates and signs the Self-Description provided by a Visitor (the future Participant/Principal). |
| Maintaining        | Trust related changes to the Self-Descriptions are recorded in a new version and validated and signed by the relevant CAB. This includes information controlled by the Participant/Principal. |
| Offboarding        | The offboarding process of a Participant is time-constrained and involves all dependent Participants/Principals. |

*Participant Lifecycle Process*

An Identity is composed of a unique Identifier and an attribute or set
of attributes that uniquely describe an entity within a given context. The lifetime of an Identifier is permanent. It may be used as a reference to an entity well beyond the lifetime of the entity it identifies or of any naming authority involved in the assignment of its name. Reuse of an Identifier for a different entity is forbidden. 
Attributes will be derived from existing identities as shown in the IAM Framework Document v1.2[^17].

[^17]: See the IAM Framwork version 1.2 for details: https://community.gaia-x.eu/s/P23ZJNLyjf7n7Zp?path=%2FReleases.

A 'Secure Digital Identity' is a unique Identity with additional data for robustly trustworthy authentication of the entity (i.e. with appropriate measures to prevent impersonation) This implies that Gaia-X Participants can self-issue Identifiers for such Identities. It is solely the responsibility of a Participant to determine the conditions under which the Identifier will be issued.  Identifiers shall be derived from the native identifiers of an Identity System without any separate attribute needed. The Identifier shall provide a clear reference to the Identity System technology used.
Additionally, the process of identifying
an Identity Holder is transparent. It must also be
possible to revoke issued Identity attributes[^18].

[^18]: For more details on Secure Identities, see Plattform Industrie 4.0: Working Group on the Security of Networked Systems. (2016). Technical Overview: Secure Identities. <https://www.plattform-i40.de/PI40/Redaktion/EN/Downloads/Publikation/secure-identities.pdf> as well as Chapter 3.4 in the IAM Framework v1.2: https://community.gaia-x.eu/s/P23ZJNLyjf7n7Zp?path=%2FReleases.

### Trust Framework

A Trust Framework is required for the Gaia-X Participants to create mutual trust between and among peers and foster Service Offerings to be provided and consumed.

This Trust framework is not enforced by the Gaia-X Association, however, only Gaia-X Participants following the policies, technical specifications, and interoperability criteria set up by the Gaia-X Association could have their Service Offerings awarded with Gaia-X Labels.

Gaia-X Association focuses on defining the policies which consider EU regulations and open technical means to provide a transparent model supporting privacy and self-determination of all Ecosystems.  The chain of trust, without the need for a global and traceable unique ID across Gaia-X is needed.

Once the Trust Framework model is in place, the Gaia-X Participants can vote and elect their own trust anchors following the rules put in place by the Association.  For example, the Gaia-X Ecosystem's Participants could agree to use the EU list of eIDAS Trusted Lists[^19] as one of the trust anchors for service providers and Conformity Assessment Bodies.

[^19]: European Commission. Trusted List Browser: Tool to browse the national eIDAS Trusted Lists and the EU List of eIDAS Trusted Lists (LOTL). <https://webgate.ec.europa.eu/tl-browser/#/>

This model allows specific Gaia-X Ecosystems to set up their own trust anchors and Federators as long as those are following the rules defined by the Gaia-X Association.
Inter-ecosystem interoperability is achieved by leveraging common GAIA-X technology while having members join each specific Federation under its own rules.
In such a model, interoperability across Ecosystems requires Participants to simultaneously be members of several Gaia-X-compatible Ecosystems / Federations.

Self-Descriptions (see section [Federated Catalogue](federation_service.md#federated-catalogue)) play another crucial part in
establishing Trust within Gaia-X. In addition to non-trust-related
information, which can be updated by the Participant, they contain
trust-related information on the Participant level, which in turn connects to the
organization's Identity System on the Principal level. The trust-related part is vetted
according to Gaia-X Policy and electronically signed by a CAB. Possible further changes regarding the trust related
information leads to a re-verification.
The Gaia-X Association in turn provides a Gaia-X Registry, which lists its policies, schemas and commonly accepted trust providers's Identities as mentioned before.

Service Offerings may have different levels of Trust. During service
composition, it is determined by the lowest trust state of the Service
Offering upon which it relies. The trust state of a Service Offering will not
affect the trust state of a Participant. On the other hand, a Policy
violation of a Participant can result in losing the trust state of its
service.

### Hybrid Identity and Access Management

The Identity and Access Management approach relies on a two-tiered approach which is currently work in progress and will be part of the next release of this document.

In practice, this means that Participants use a selected few Identity Systems for mutual identification and trust establishment, SSI being the recommended option for interoperability. After trust is established, underlying existing technologies already in use by Participants (on the “Principal level”) can be federated and reused, for example Open ID Connect or domain specific x509-based communication protocols.

Gaia-X Participants might need to comply with additional requirements on the type and
usage of credentials management applications such as mandatory
minimum-security requirements, such as Multi-factor authentication.
Server-to-Server Communication plays a crucial role in Gaia-X and the
integration of self-sovereignty must be worked out in more detail.

#### Federated Trust Model

The Federated Trust Model relies on the Gaia-X Federated Trust Component, which queries and verifies trust related information (like Gaia-X Labels, domain specific certifications) of  Participants to determine whether they meet other Participants’ respective trust requirements.


## Data Sovereignty Services

Data Sovereignty Services provide Participants the capability to have full self-determination of the exchange and sharing of their
data. They can also decide to act without having the Data Sovereignty Service involved, if they wish to do so.

Informational self-determination for all Participants includes two
aspects within the Data Ecosystem: (1) Transparency, and (2) Control 
of data usage. Enabling Data Sovereignty when exchanging,
sharing and using data relies on fundamental functions and capabilities
that are provided by Federation Services in conjunction with other
mechanisms, concepts, and standards. The Data Sovereignty Services build
on existing concepts of usage control that extend traditional access
control. Thus, usage control is concerned with requirements that pertain
to future data usage patterns (i.e., obligations), rather than data access
(provisions).

### Capabilities for Data Sovereignty Services

The foundation for Data Sovereignty is a trust-management mechanism to
enable a reliable foundation for peer-to-peer data exchange and usage,
but also to enable data value chains involving multiple Providers and
Consumers. All functions and capabilities can be extended
and configured based on domain-specific or use case-specific
requirements to form reusable schemes.

The following are essential capabilities for Data Sovereignty in the
Gaia-X Data Ecosystems:

| Capability                                        | Description |
|---------------------------------------------------|-------------|
| Expression of Policies in a machine-readable form | To enable transparency and control of data usage, it is important to have a common policy specification language to express data usage restrictions in a formal and technology-independent manner that is understood and agreed by all Gaia-X Participants. Therefore, they must be formalized and expressed in a common standard such as ODRL[^23]. |
| Inclusion of Policies in Self-Descriptions         | Informational self-determination and transparency require metadata to describe Resources as well as Providers, Consumers, and Usage Policies as provided by Self-Descriptions and the Federated Catalogues. |
| Interpretation of Usage Policies                  | For a Policy to be agreed upon, it must be understood and agreed by all Participants in a way that enables negotiation and possible technical and organizational enforcement of Policies. |
| Enforcement                                       | Monitoring of data usage is a detective enforcement of data usage with subsequent (compensating) actions. In contrast, preventive enforcement[^24] ensures the policy Compliance with technical means (e.g., cancellation or modification of data flows). |

*Capabilities for Gaia-X Data Sovereignty Services*

[^23]: W3C. ODRL Information Model 2.2 [W3C Recommendation 15 February 2018]. <https://www.w3.org/TR/odrl-model/>
[^24]: Currently not in scope of Gaia-X Federation Services

### Functions of Data Sovereignty Services

Information services provide more detailed information about the general
context of the data usage transactions. All information on the data
exchange and data usage transactions must be traceable;  therefore, agreed monitoring
and logging capabilities are required for all data usage transactions. Self-determination also means that Providers can choose to
apply no Usage Policies at all.

The Data Sovereignty Services in Gaia-X implement different functions
for different phases of the data exchanges. Therefore, three distinct phases of
data exchanges are defined:

-   before transaction
-   during transaction
-   after transaction

Before the data exchange transaction, the Data Agreement Service is
triggered and both parties negotiate a data exchange agreement. This
includes Usage Policies and the required measures to implement those.
During transactions, a Data Logging Service receives logging-messages
that are useful to trace each transaction. This includes data provided,
data received, policy enforced, and policy-violating messages. During
and after the transaction the information stored can be queried by the
transaction partners and a third eligible party, if required. The figure below
shows the role of the aforementionentioned services to enable sovereign data exchange.

![](figures/image9.png)
*Data Sovereignty Services Big Picture*

The Data Agreement Service enables data transactions in a secure,
trusted, and auditable way. It offers interfaces for the negotiation
detailing the agreed terms for planned data exchange. The service is not
meant to handle the transaction of data (which is described in the
negotiated data contracts).

The Data Logging Service provides evidence that data has been (a)
transmitted, (b) received and (c) that rules and obligations (Usage
Policies) were successfully enforced or were violated. This supports the clearing of
operational issues but also identifies fraudulent transactions.

The Provider can track if, how, and what data was provided, with the
Consumer being notified about this. The Consumer can track if data was
received or not, and, additionally, track and provide evidence on the
enforcement or violation of Usage Policies.

## Compliance

Gaia-X defines a Compliance framework that manifests itself in the form of
a code of conduct, third party Certifications / attestations, or
acceptance of Terms and Conditions. It is detailed in the Policy Rules document. 
Requirements from the field of security (e.g., data
encryption, protection, or interoperability) form the basis for this
Compliance framework. The main objective of Federation Services
Compliance is to provide Gaia-X users with transparency on the
Compliance of each specific Service Offering.

Federation Services consist of two components: First, the
Onboarding and Accreditation Workflow (OAW) that ensures that all
Participants, Resources and Service Offerings undergo a
validation process before being added to a Catalogue; Second, the
Continuous Automated Monitoring (CAM) that enables monitoring of the
Compliance based on Self-Descriptions. This is achieved by automatically
interacting with the service-under-test, using standardised protocols
and interfaces to retrieve technical evidence. One goal of the OAW is to
document the validation process and the generation of an audit trail to
guarantee adherence to generally accepted practices in Conformity
Assessments. In addition to the general onboarding workflow, special functions must include:

-   Monitoring of the relevant bases for Compliance
-   Monitoring of updates to Service Offerings that could trigger revisions / recertifications for Compliance
-   Suspension of Service Offerings
-   Revocation of Service Offerings

## Gaia-X Portals and APIs

The Gaia-X Portals support Participants to interact with 
Federation Services functions via a user interface, which provides
mechanisms to interact with core capabilities using API calls. The goal
is a consistent user experience for all tasks that can be performed with
a specific focus on security and Compliance. The Portals provide
information on Resources and Service Offerings and interaction
mechanisms for tasks related to their maintenance. Each Ecosystem can 
deploy its own Portals to support interaction with Federation Services. 
The functions of the Portals are further described below.

A Portal supports the registration of organizations as new Participants.
This process provides the steps to identify and authorize becoming a
Participant. Additionally, organizations are assisted in registering as
members of the Gaia-X association AISBL. Participants are supported in managing
Self-Descriptions and organizing Credentials. This includes
Self-Description revisions and administration. A Portal further offers
search and filtering of Service Offerings and Participants, based on
Federated Catalogues. Additionally, solution packaging refers to a
composition mechanism for the selection and combination of Service
Offerings into solution packages to address specific use cases possible
with a Portal. To orchestrate the various APIs, an API framework to
create a consistent user and developer experience for API access and
lifecycle is introduced. An API gateway will ensure security for all
integrated services. An API portal will provide a single point of
information about available API services and version management. 
