# Changelog

## 2021 September release

- Rewrite of the [Operating model](operating_model.md) chapter introducing Trust Anchors, Gaia-X Compliance, Gaia-X Labels and Gaia-X Registry.
- Update of Self-Description mandatory attributes in the [Appendix](appendix.md#a3).
- Update of `Interconnection`, `Resource` and `Resource template` definitions.
- Gitlab automation improvement and speed-up
- Source available in the [21.09](https://gitlab.com/gaia-x/gaia-x-technical-committee/gaia-x-architecture-document/-/tree/21.09) branch.

## 2021 June release

- Adding a new [Operating model](operating_model.md) section introducing the first principle for Gaia-X governance.
- Adding preview of Self-Description mandatory attributes in the [Appendix](appendix.md#a3).
- Improvement of the [Policy rules](conceptual_model.md#policies).
- Improvement of the `Asset` and `Resource` definitions.
- Complete release automation from Gitlab.
- Source available under the [21.06](https://gitlab.com/gaia-x/gaia-x-technical-committee/gaia-x-core-document-technical-concept-architecture/-/tree/21.06) tag.

## 2021 March release

- First release of the Architecture document by the [Gaia-X Association AISBL](https://www.gaia-x.eu/)
- Complete rework of the Gaia-X [Conceptual Model](conceptual_model.md) with new entities' definition.
- Adding a [Glossary](glossary.md) section
- Source available under the [21.03-markdown](https://gitlab.com/gaia-x/gaia-x-technical-committee/gaia-x-core-document-technical-concept-architecture/-/tree/21.03-markdown) tag.

## 2020 June release

- First release of the Technical Architecture document by the [BMWi](https://www.bmwi.de/Navigation/DE/Home/home.html)
