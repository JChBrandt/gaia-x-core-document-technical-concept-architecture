
function onlyUnique(value, index, self) {
  return self.indexOf(value) === index;
}

window.onload = function() {
  let count = Array.from(document.querySelectorAll('a.merge-request')).map(e => e.href).filter(onlyUnique).length;
  let counter = document.createElement('span');
  counter.innerText = ' (' + count + ' open merge-requests)';
  document.querySelector('h1').insertAdjacentElement("beforeend", counter);
}
