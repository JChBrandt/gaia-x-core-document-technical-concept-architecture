#!/usr/bin/env python3

import skimage.color
import numpy as np
from functools import partial
import sys
import xml.etree.ElementTree as ET

GXBLUE = (0, 0, 148)
GXDARKBLUE = (0, 0, 113)
GXMIDBLUE = (70, 90, 255)
PURPLE = (185, 0, 255)
TURQUOISE = (70, 218, 255)
DARK = (0, 0, 0)
WHITE = (255, 255, 255)
COLORS = [GXBLUE, GXDARKBLUE, GXMIDBLUE, PURPLE, TURQUOISE] #, DARK]

def remove_alpha(color, opacity, bg_color=(255, 255, 255)):
### from https://filosophy.org/code/online-tool-to-lighten-color-without-alpha-channel/
    return (int(opacity * color[0] + (1 - opacity) * bg_color[0]),
    int(opacity * color[1] + (1 - opacity) * bg_color[1]),
    int(opacity * color[2] + (1 - opacity) * bg_color[2]))

def distance(col1, col2):
    deltaE = np.sqrt((col1[0] - col2[0])**2 + (col1[1] - col2[1])**2 + (col1[2] - col2[2])**2)
    return deltaE

def distance_palette(palette, col2):
    return list(map(partial(distance, col2), palette))

def rgb2lab(rgb):
    return skimage.color.rgb2lab([[np.divide(rgb, 255)]])[0][0]

def lab2rgb(lab):
    return skimage.color.lab2rgb([[lab]])[0][0]*255

def rgb2hsv(rgb):
    return skimage.color.rgb2hsv(np.divide(rgb, 255))

def hsv2rgb(hsv):
    return skimage.color.hsv2rgb([[hsv]])[0][0]*255

def hex2rgb(hex):
    assert hex.startswith('#')
    assert len(hex) == 7 or len(hex) == 4
    if len(hex) == 7:
        return (int(hex[1:3], 16), int(hex[3:5], 16), int(hex[5:7], 16))
    return (int(hex[1:2], 16), int(hex[2:3], 16), int(hex[3:4], 16))

def rgb2hex(rgb):
    return '#{:02x}{:02x}{:02x}'.format(int(rgb[0]), int(rgb[1]), int(rgb[2]))

def fix_gray(hex_in, hex_out):
    rgb_in = hex2rgb(hex_in)
    if rgb_in[0] == rgb_in[1] and rgb_in[0] == rgb_in[2]: # this B&W
        return hex_in
    return hex_out

def find_color(hex_in):
    lab_in = rgb2lab(hex2rgb(hex_in))
    idx = np.argmin(distance_palette(palette_lab, lab_in))
    hex_out = palette_hex[idx]
    hsv_out = rgb2hsv(hex2rgb(hex_out))
    hsv_out[1] = min(hsv_out[1]*5, 1) # increase saturation by 500%
    lab_out = rgb2lab(hsv2rgb(hsv_out))
    lab_out[0] = lab_in[0] # keep same L*
    hex_out = rgb2hex(lab2rgb(lab_out))
    hex_out = fix_gray(hex_in, hex_out) # if in is B&W, keep B&W
    print(hex_in, "->", hex_out)
    return hex_out


palette_rgb = [WHITE]
gradient_step = 5 # limit to few colors in the overall palette
for color in COLORS:
    for i in range(1, gradient_step+2):
        opacity = i*1./(gradient_step+1 + gradient_step/3) # +gradient_step/3: hack to avoid dull (gray-ish) color
        palette_rgb.append(remove_alpha(color, opacity))


palette_lab = []
palette_hex = []
for color in palette_rgb:
    palette_lab.append(rgb2lab(color))
    palette_hex.append(rgb2hex(color))

assert np.argmin(distance_palette(palette_lab, palette_lab[0])) == 0
assert np.argmin(distance_palette(palette_lab, palette_lab[-1])) == len(palette_lab) - 1

if __name__ == "__main__":
    for filename in sys.argv[1:]:
        print(filename)
        assert filename.endswith('.svg')
    ET.register_namespace("","http://www.w3.org/2000/svg")
    tree = ET.parse(filename)
    root = tree.getroot()
    for child in root.iter():
        # print(child.tag, child.attrib)
        if 'fill' in child.attrib:
            child.attrib['fill'] = find_color(child.attrib['fill'])
        if 'style' in child.attrib:
            styles = child.attrib['style'].split(";")
            for i, style in enumerate(styles):
                if style.startswith('fill:'):
                    styles[i] = f"fill:{find_color(style[5:])}"
            child.attrib['style'] = ";".join(styles)

    filename_out = filename[:-4] + "-gx.svg"
    with open(filename_out, mode='w', encoding='utf-8') as fd:
        fd.write(ET.tostring(root, encoding="unicode"))
